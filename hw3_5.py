import plotly.graph_objects as go
import numpy as np
from plotly.subplots import make_subplots

xB= np.arange(0.01, 1.01, step=0.01)
omg_p = 0.5
T_0 = 1000
xA = 1-xB

fig = make_subplots(1, 1)
temp_range = np.arange(1000, 2000, 50)

for T in temp_range:
        fig.add_scatter(x = xB,
                        y = xA + 0.9*xB + omg_p*xA*xB + ((T_0-T)/T_0)*xA*xB + (xB - (1/2))**4,
                        row=1, 
                        col=1, 
                        visible=True,
                        line=dict( width=4, dash="solid"),
                        name= 'G, '+ str(T) + 'K')

fig.update_xaxes(
    range=(0.01, 0.99),
    constrain='domain'
)
fig.update_layout(title='',
                  xaxis_title='$xB$',
                  yaxis_title='$G$',
                  showlegend=False)



temp = []
point1 = []
point2 = []
tol = 0.001
xB= np.arange(0.11, 0.39, step=0.01)
xB2= np.arange(0.53, 1.01, step=0.01)

temp_range = np.arange(1000, 2100, 20)

for T in temp_range:
    mu1 = -1 + 0.9 + omg_p*(1 - 2*xB) + ((T_0-T)/T_0)*(1- 2*xB) + 4*(xB- (1/2))**3
    for x in reversed(xB2):
        mu2 = -1 + 0.9 + omg_p*(1 - 2*x) + ((T_0-T)/T_0)*(1- 2*x) + 4*(x- (1/2))**3
        for i in np.arange(1,len(xB)):
            err = np.abs(mu1[i]-mu2)
            if T in temp:
                continue
            if err <= tol:
                point1.append(xB[i])
                point2.append(x)
                temp.append(T)
                continue
            
fig = make_subplots(1, 1)

for T in temp:
    fig.add_scatter(x = [point1[temp.index(T)], point2[temp.index(T)]],
                    y = [T, T],
                        row=1, 
                        col=1, 
                        visible=True,
                        mode = 'markers',
                        line=dict(color="red", width=4, dash="solid"),
                        name= str(T) + 'K')

fig.update_layout(title='Phase Diagram',
                  xaxis_title='$xB$',
                  yaxis_title='$T$',
                  showlegend=False)

fig.show()
