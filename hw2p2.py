# import libraries
import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objects as go
import pandas as pd

def alpha_theo(Tb,T):
    alpha = (1/2)*((Tb - T)**(-1/3)) * ( (1/12)*((Tb + np.sqrt(T*Tb))**(-5/6)) * ((T*Tb)**(-1/2))
                                             * Tb *((Tb - T)**(1/6))
                                            + (1/6)*((Tb - T)**(-5/6)) *
                                             ( (Tb + np.sqrt(T*Tb))**(1/6) + (Tb - np.sqrt(T*Tb))**(1/6))
                                            )
    return alpha

# Experimental Values at 300K

T = 300
elem = np.array([['Li', 'Al', 'Fe', 'W']])
Tb_exp = np.array([[1615, 2793, 3135, 5828]])
alpha_exp = np.array([[46, 23, 12, 4.5]])

my = np.hstack((elem.T, Tb_exp.T, alpha_exp.T))

df = pd.DataFrame(my, columns=['element', 'Tb', 'a_exp'])
df['Tb'] = df['Tb'].astype(float)
df['a_exp'] = df['a_exp'].astype(float)

theory = []
for i in range(4):
    print(alpha_theo(df['Tb'].values[i], T) * 10**6)
    theory.append(alpha_theo(df['Tb'].values[i], T) * 10**6)


# plot the interatomic Potential
fig = go.Figure()
fig.add_trace(go.Scatter(x=df['Tb'], y=df['a_exp'], name='Experiment',
                         line=dict(color='darksalmon', width=4, dash='dot'),
                         mode="lines+markers+text",
                         marker=dict(size=12),
                         text=df['element'],
                         textposition="top center",
                         textfont=dict(
                                    family="sans serif",
                                    size=30,
#                                    color="LightSeaGreen"
                                        )
                        )
             )
fig.add_trace(go.Scatter(x=df['Tb'], y=df['a_theory'], name='Theory',
                         line=dict(color='indigo', width=4, dash='dot'),
                         mode="lines+markers+text",
                         marker=dict(size=12),
                         text=df['element'],
                         textposition="bottom center",
                         textfont=dict(
                                    family="sans serif",
                                    size=30,
                                       )
                        )
             )

# dash options include 'dash', 'dot', and 'dashdot'
fig.update_layout(title='Thermal expansion',
                   xaxis_title='Temperature',
                   yaxis_title='Expansion Coefficient',
                 font=dict(family="sans serif", size=20))

fig.show()
